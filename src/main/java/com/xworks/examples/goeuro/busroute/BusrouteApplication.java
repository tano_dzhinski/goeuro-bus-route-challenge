package com.xworks.examples.goeuro.busroute;

import javax.inject.Singleton;

import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xworks.examples.goeuro.busroute.core.BusrouteService;
import com.xworks.examples.goeuro.busroute.core.Routes;
import com.xworks.examples.goeuro.busroute.core.exception.RouteException;
import com.xworks.examples.goeuro.busroute.resources.BusRoutesResource;
import com.xworks.examples.goeuro.busroute.route.text.RouteSourceFactory;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class BusrouteApplication extends Application<BusrouteConfiguration> {

	private String fileLocation;

	final static Logger logger = LoggerFactory.getLogger(BusrouteApplication.class);

	public static void main(final String[] args) throws Exception {
		new BusrouteApplication().run(args);
	}

	@Override
	public String getName() {
		return "busroute";
	}

	@Override
	public void initialize(final Bootstrap<BusrouteConfiguration> bootstrap) {
	}

	@Override
	public void run(String... arguments) throws Exception {

		if (arguments.length < 3) {
			System.err.println("Missing route file path.");
			System.exit(1);
		}

		fileLocation = arguments[2];

		// pass server command args
		super.run(new String[] { arguments[0], arguments[1] });
	}

	@Override
	public void run(final BusrouteConfiguration configuration, final Environment environment) {
		environment.jersey().register(new BusRoutesResource());
		environment.jersey().register(new AbstractBinder() {

			@Override
			protected void configure() {
				bind(BusrouteService.class).to(BusrouteService.class);
				bindFactory(new Factory<Routes>() {

					@Override
					public Routes provide() {
						Routes routes = null;
						try {
							routes = RouteSourceFactory.getRoutes(fileLocation);
							routes.init();
						} catch (RouteException e) {
							logger.error(e.getMessage());
						}

						return routes;
					}

					@Override
					public void dispose(Routes instance) {
					}
				}).to(Routes.class).in(Singleton.class);
			}
		});
	}
}
