package com.xworks.examples.goeuro.busroute.core.exception;

public class RouteException extends Exception {

	public RouteException(Exception e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4354605538424910715L;
	
	@Override
	public String getMessage() {
		return "Error searching for a route.";
	}

}
