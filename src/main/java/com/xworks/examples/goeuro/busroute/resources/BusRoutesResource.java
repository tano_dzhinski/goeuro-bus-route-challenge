package com.xworks.examples.goeuro.busroute.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.process.internal.RequestScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xworks.examples.goeuro.busroute.api.DirectRouteResponse;
import com.xworks.examples.goeuro.busroute.core.BusrouteService;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class BusRoutesResource {

	final static Logger logger = LoggerFactory.getLogger(BusRoutesResource.class);

	@Inject
	private BusrouteService busrouteService;

	@GET()
	@Path("direct")
	public Response getDirectPath(@QueryParam("dep_sid") int departureSid, @QueryParam("arr_sid") int arrivalSid) {

		DirectRouteResponse directRouteResponse = new DirectRouteResponse();
		directRouteResponse.setDepSid(departureSid);
		directRouteResponse.setArrSid(arrivalSid);

		boolean exists = busrouteService.directRouteExists(departureSid, arrivalSid);

		directRouteResponse.setDirectBusRoute(exists);
		return Response.ok(directRouteResponse).build();
	}
}
