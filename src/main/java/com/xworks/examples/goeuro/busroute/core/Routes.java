package com.xworks.examples.goeuro.busroute.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.xworks.examples.goeuro.busroute.core.exception.RouteException;

public abstract class Routes {

	private Map<Integer, ArrayList<Integer>> stations = new HashMap<>();

	public void init() throws RouteException {
		Route route;

		while ((route = getNextRoute()) != null) {

			Integer routeId = route.getId();
			Collection<Integer> routeStations = route.getStations();

			// update stations index
			for (Integer stationId : routeStations) {
				if (stations.containsKey(stationId)) {
					stations.get(stationId).add(routeId);
				} else {
					ArrayList<Integer> routes = new ArrayList<>();
					routes.add(routeId);
					stations.put(stationId, routes);
				}
			}
		}
	}

	protected abstract Route getNextRoute() throws RouteException;


	/**
	 * Get all routes containing this station
	 * 
	 * @param id
	 * @return Collection of route Ids
	 */

	public Collection<Integer> getRoutes(int id) {
		return stations.get(id);
	}

	protected class Route {
		private int id;
		private ArrayList<Integer> stations;

		public Route() {
			stations = new ArrayList<>();
		}

		public Collection<Integer> getStations() {
			return stations;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public void addStation(Integer stationId) {
			stations.add(stationId);
		}
	}
}
