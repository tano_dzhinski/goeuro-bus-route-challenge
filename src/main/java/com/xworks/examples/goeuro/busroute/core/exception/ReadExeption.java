package com.xworks.examples.goeuro.busroute.core.exception;

import java.io.IOException;

public class ReadExeption extends Exception {

	public ReadExeption() {
		super();
	}

	public ReadExeption(IOException e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -760486569796615380L;

}
