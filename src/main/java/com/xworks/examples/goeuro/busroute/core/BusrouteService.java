package com.xworks.examples.goeuro.busroute.core;

import java.util.Collection;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BusrouteService {

	final static Logger logger = LoggerFactory.getLogger(BusrouteService.class);

	@Inject
	private Routes routes;

	public boolean directRouteExists(int departureSid, int arrivalSid) {

		Collection<Integer> depRoutes = routes.getRoutes(departureSid);
		Collection<Integer> arrRoutes = routes.getRoutes(arrivalSid);

		if ((depRoutes == null) || (arrRoutes == null)) {
			logger.debug("Not found [{}, {}] - {}, {}", departureSid, arrivalSid, depRoutes, arrRoutes);
			return false;
		}

		// is there common routes?
		for (Integer arrRoute : arrRoutes) {
			for (Integer depRoute : depRoutes) {
				if (arrRoute == depRoute) {
					logger.debug("Common routes [{}, {}] - {}", departureSid, arrivalSid, depRoutes);
					return true;
				}
			}
		}

		logger.debug("Not found [{}, {}]", departureSid, arrivalSid);
		return false;
	}
}
