package com.xworks.examples.goeuro.busroute.core.exception;

public class RouteSourceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5269677789409860636L;
	
	public RouteSourceException(Throwable cause) {
		super(cause);
	}
}
