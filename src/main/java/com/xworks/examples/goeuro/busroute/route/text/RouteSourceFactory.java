package com.xworks.examples.goeuro.busroute.route.text;

import com.xworks.examples.goeuro.busroute.core.Routes;
import com.xworks.examples.goeuro.busroute.core.exception.RouteException;

public class RouteSourceFactory {

	public static Routes getRoutes(String fileLocation) throws RouteException {
		return new TextFileRoutes(fileLocation);
	}
}
