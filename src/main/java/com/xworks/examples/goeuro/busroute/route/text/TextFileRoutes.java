package com.xworks.examples.goeuro.busroute.route.text;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.xworks.examples.goeuro.busroute.core.Routes;
import com.xworks.examples.goeuro.busroute.core.exception.RouteException;

class TextFileRoutes extends Routes {

	private BufferedReader reader;

	public TextFileRoutes(String filePath) throws RouteException {
		try {
			reader = new BufferedReader(new FileReader(filePath));
			// skip routes count ??? :(
			reader.readLine();
		} catch (IOException e) {
			try {
				reader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			throw new RouteException(e);
		}
	}

	@Override
	protected Route getNextRoute() throws RouteException {

		String line = null;
		try {
			line = reader.readLine();
			if (line == null) {
				reader.close();
				return null;
			}
		} catch (IOException e) {
			try {
				reader.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw new RouteException(e);
		}

		Route route = new Route();

		String[] strings = line.split("\\s+");

		for (int i = 0; i < strings.length; i++) {
			if (i == 0) {
				route.setId(new Integer(strings[i]));
			} else {
				route.addStation(new Integer(strings[i]));
			}
		}

		return route;
	}

	@Override
	protected void finalize() throws Throwable {
		if (reader != null) {
			reader.close();
		}
	}
}
